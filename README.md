tinysoul
========

Description
-----------

This is a small netsoul client which come without any dependance.
Netsoul is a school auth protocol.
The whole code is done in C++11

Compile
-------

To compile this program just do the following :

./configure
cd build
make

see configure --help for more information on the availables build

Configure
---------

you can either install it or use it in local folder.

you need to fill the configuration in doc/tinysoul.conf and move it in the
following folder for autodetection or give the path to tinysoul.

 - ./tinysoul.conf
 - ~/.tinysoul.conf
 - /etc/tinysoul.conf

Run
---

To run the program fill the configuration file which is in the folder doc, if
you have a jogsoul configuration it will also work.
without arguments it will search for a tinysoul.conf just near the binary

see tinysoul -h for more information on the available options

Author
------

Samuel 'Staphylo' Angebault <staphyloa@gmail.com>

License
-------

This is beerware copylefted
