#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
#include "socket.hpp"
#include "md5.hpp"

extern "C" {
#include <sys/select.h>
}

#include "misc.hpp"
#include "netsoul.hpp"

Netsoul::Netsoul(Config& conf) : connected_(false), conf_(conf)
{
}

Netsoul::~Netsoul()
{
    disconnect();
}

void Netsoul::disconnect()
{
    if (connected_)
    {
        sock_.send("exit\n");
        connected_ = false;
    }
}

bool Netsoul::connect() throw (const char *)
{
    log() << "Connecting to " << conf_.server << ":"
              << conf_.port << std::endl;
    if(!sock_.create())
        throw "Socket Error";

    hostent *hst = gethostbyname(conf_.server.c_str());
    if(!hst)
        throw "Error getting host name";

    in_addr *addr = (in_addr *)hst->h_addr;
    log() << "at " << inet_ntoa(*addr) << ":" << conf_.port << std::endl;

    if(sock_.connect(inet_ntoa(*addr), conf_.port))
        log() << "Connection Successful" << std::endl;
    else
        throw "Connection error";

    connected_ = true;
    return true;
}

std::string Netsoul::read()
{
    std::string res;
    sock_.recv(res);
    log() << "<< " << res << std::endl;
    return res;
}

void Netsoul::send(std::string str)
{
    log() << ">> " << str << std::endl;
    sock_.send(str + "\n");
}

// recommended states awailable :
// actif, away, idle, connection, lock, server, none
// possible with : user_cmd state <state>
void Netsoul::change_state(std::string state)
{
    std::stringstream stream;
    stream << "state " << state << ":" << time(0);
    send(stream.str());
    log() << "Changing state : " << stream.str() << std::endl;
}

std::string Netsoul::read_timeout(int delay)
{
    fd_set masterfds;
    fd_set readfds;
    struct timeval timeout;

    FD_ZERO(&masterfds);
    FD_SET(sock_.fd(), &masterfds);
    std::memcpy(&readfds, &masterfds, sizeof(fd_set));
    timeout.tv_sec = delay;
    timeout.tv_usec = 0;

    if (select(sock_.fd() + 1, &readfds, NULL, NULL, &timeout) < 0)
        throw "select error";

    if (FD_ISSET(sock_.fd(), &readfds))
        return read();
    else
        throw "Connection timed out";
}

void Netsoul::loop()
{
    std::string str = read_timeout(SOCKET_TIMEOUT);
    if (str.empty())
        throw "Empty data received";

    std::vector<std::string> array;
    size_t len = split(array, str, ' ');

    // ping <id>
    if(array[0] == "ping" && len >= 2)
    {
        send(str);
    }
    // msg <>
    else if(array[0] == "msg" && len >= 10)
    {
    }
}

bool Netsoul::auth()
{
    std::vector<std::string> array;

    std::string welcome = read();
    send("auth_ag ext_user none none");
    std::string rep = read();

    int len = split(array, welcome, ' ');
    if (len != 6)
    {
        log() << "Invalid salut received" << std::endl;
        return false;
    }

    if(rep != "rep 002 -- cmd end")
    {
        log() << "Authentification query failed" << std::endl;
        return false;
    }

    // secret - host / port password
    std::string pass = array[2] + "-" + array[3] + "/" + array[4] +
                       conf_.password;
    std::string md5hash = MD5(pass).hexdigest();

    std::string cmd = "ext_user_log " + conf_.login + " "
                               + md5hash + " "
                               + conf_.location + " "
                               + conf_.data;
    str_to_lower(cmd);
    send(cmd);
    rep = read();
    if(rep != "rep 002 -- cmd end")
    {
        log() << "Authentification failed" << std::endl;
        return false;
    }

    change_state("server");

    log() << "Authentification succeed" << std::endl;
    return true;
}
