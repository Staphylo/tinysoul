#ifndef MISC_H
#define MISC_H

#include <string>
#include <vector>

using namespace std;

// end the program with an error message
void die(string str);

// replace the search pattern by replace in the string
string &str_replace(const string &search, const string &replace, string &str);

// Split a string to a vector of string where the delimiter is
int split(vector<string> &vector, string str, char delimiter);

// Put the whole string to lower case
void str_to_lower(string &str);

// Encode a message in url type
string &url_encode(string &str);

// Decode a message of url type
string &url_decode(string &str);

void daemonize();

void logfile(const char *filename);

std::ostream& log();

// For internal use...
//
// int hex_char_to_int(char c);
// char string_hex_to_char(string str); 

#endif /* !MISC_H */
