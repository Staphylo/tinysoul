#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <ctime>

extern "C" {
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
}

#include "misc.hpp"

static const char *chrootdir = "/";
static bool log_enable = false;
static std::ofstream logstream;

void die(string str)
{
    cerr << str << endl;
    exit(1);
}


string &str_replace(const string &search, const string &replace, string &str)
{
    string buffer;

    int sealen = search.length();
    int strlen = str.length();
    int replen = replace.length();

    if(sealen == 0)
        return str;

    for(int i = 0, j = 0; i < strlen ; j = 0)
    {
        while(i + j < strlen && j < sealen && str[i+j] == search[j])
            j++;

        if(j == sealen)
        {
            buffer.append(replace);
            i += replen;
        }
        else
        {
            buffer.append(&str[i], 1);
            i++;
        }
    }
    str = buffer;
    return str;
}

string &url_encode(string &str)
{
    stringstream buf;
    int strlen = str.length();
    
    if(strlen == 0)
        return str;

    for(int i = 0 ; i < strlen ; i++)
    {
        if( (str[i] < 'a' || str[i] > 'z') &&
            (str[i] < '0' || str[i] > '9') &&
            (str[i] < 'A' || str[i] > 'Z') )
        {
            buf << '%' << hex << (int)str[i]; 
        }
        else
        {
            buf << str[i];
        }
    }

    str = buf.str();
    return str;
}


int hex_char_to_int(char c)
{
    if(c >= 'a' && c <= 'f')
        return 10 + c - 'a';
    else if(c > '0' && c <= '9')
        return c - '0';

    return 0;
}

char string_hex_to_char(string str)
{
    if(str.length() > 10)
        return 0;

    char c_high = str[0];
    char c_low = str[1];
    
    int i_high = hex_char_to_int(c_high);
    int i_low = hex_char_to_int(c_low);

    return (char)(i_high * 16 + i_low);
}

string &url_decode(string &str)
{
    stringstream buf;
    int strlen = str.length();

    if(strlen == 0)
        return str;

    for(int i = 0 ; i < strlen ; i++)
    {
        if(str[i] == '%')
        {
            buf << (char)string_hex_to_char(str.substr(i+1, 2));
            i += 2;
        }
        else
        {
            buf << str[i];
        }
    }

    str = buf.str();
    return str;
}

int split(vector<string> &vector, string str, char delimiter)
{
    vector.clear();
    string::size_type pos = str.find(delimiter);

    while(pos != string::npos)
    {
        vector.push_back(str.substr(0, pos));
        str = str.substr(pos+1);
        pos = str.find(delimiter);
    }

    vector.push_back(str);

    return vector.size();
}


void str_to_lower(string &str)
{
    string::size_type strlen = str.length();
    for(string::size_type i = 0 ; i < strlen ; i++)
        if(str[i] >= 'A' && str[i] <= 'Z')
            str[i] = (char)(str[i] - 'A' + 'a');
}


void daemonize()
{
    log() << "Daemonizing client" << std::endl;

    umask(0);

    pid_t pid;
    if ((pid = fork()) < 0)
        log() << "/!\\ Fork failed /!\\" << std::endl;
    else if (pid != 0)
        exit(0);

    setsid();

    if (!chrootdir && chdir(chrootdir) < 0)
        log() << "/!\\ Couldn't chdir /!\\" << std::endl;

    //dup2(0, open("/dev/null", O_RDONLY));
    //dup2(1, open(logpath, O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR));
    //dup2(2, open(logpath, O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR));
    log() << "Now being daemonized" << std::endl;
}

void logfile(const char *filename)
{
    logstream.open(filename);
    if (logstream.good())
        log_enable = true;
}

std::ostream& log()
{
    time_t t = time(0);
    tm *ti = localtime(&t);
    std::ostream& out = (log_enable) ? logstream : std::cout;

    out << "[" << std::setw(2) << std::setfill('0')
              << ti->tm_hour << ":"
              << std::setw(2) << ti->tm_min << ":"
              << std::setw(2) << ti->tm_sec << "] ";

    return out;
}
