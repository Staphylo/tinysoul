#include <iostream>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>

#include "config.hpp"
#include "misc.hpp"

Config::Config(std::string filename)
{
    std::ifstream file(filename, std::ios::in);
    std::string line;

    if(!file.is_open())
        die("Configuration file not found");
    else
        log() << "Configuration loaded from " << filename << endl;

    while(getline(file, line))
    {
        std::string::size_type i = line.find_first_not_of(" \n\v\t");
        std::string::size_type p;

        if(i >= string::npos || line[i] == '#')
            continue;
        else if(line[i] == ':')
        {
            p = line.find_last_not_of(" \n\v\t");
            string key = line.substr(i+1, p);
            if(key == "server")
            {
                getline(file, server);
            }
            else if(key == "port")
            {
                getline(file, line);
                port = atoi(line.c_str());
            }
            else if(key == "login")
            {
                getline(file, login);
            }
            else if(key == "password" || key == "pass")
            {
                getline(file, password);
            }
            else if(key == "location")
            {
                getline(file, location);
                url_encode(location);
            }
            else if(key == "data")
            {
                getline(file, data);
                url_encode(data);
            }
        }
    }

    file.close();
}

bool Config::check() const
{
    bool valid = true;

    if(server.empty())
    {
        cout << "Server not set" << endl;
        valid = false;
    }

    if(port == 0)
    {
        cout << "Port not set" << endl;
        valid = false;
    }

    if(login.empty())
    {
        cout << "Login not set" << endl;
        valid = false;
    }

    if(password.empty())
    {
        cout << "Password not set" << endl;
        valid = false;
    }

    return valid;
}
