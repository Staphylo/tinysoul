#ifndef NETSOUL_HPP_
# define NETSOUL_HPP_

# include <string>

# include "config.hpp"
# include "socket.hpp"

# define PING_TIMEOUT_MINUTE 11
# define PING_TIMEOUT_SECOND 10
# define SOCKET_TIMEOUT (60 * PING_TIMEOUT_MINUTE + PING_TIMEOUT_SECOND)
# define RECONNECTION_TIMEOUT 5


class Netsoul
{
public:
    Netsoul(Config& conf);
    ~Netsoul();

    bool connect() throw (const char *);
    void loop();
    bool auth();
    void disconnect();
    void change_state(std::string state);

private:
    void parse();
    void send(std::string str);
    std::string read();
    std::string read_timeout(int delay);

    bool connected_;
    Config& conf_;
    Socket sock_;
};

#endif /* !NETSOUL_HPP_ */
