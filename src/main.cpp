#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "misc.hpp"
#include "netsoul.hpp"
#include "config.hpp"

std::vector<std::string> config_paths = {
    "./",
    "~/.",
    "/etc/",
    "/usr/local/etc/",
};

std::string configfile = "./tinysoul";
bool isdaemon = false;

static void link(Config& cfg)
{
    static bool first = true;

    Netsoul netsoul(cfg);
    if (!netsoul.connect())
        return;
    if (!netsoul.auth())
        return;

    if (first && isdaemon)
    {
        daemonize();
        first = false;
    }

    do
    {
        try
        {
            netsoul.loop();
        }
        catch(const char *msg)
        {
            log() << msg << std::endl;
            break;
        }
    } while(true);

    log() << "Connection lost" << std::endl;
}

static void usage(const char *prg)
{
    std::cout << "Usage: " << prg << " [-d] [-h] [config]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "   -d            daemonize the client" << std::endl;
    std::cout << "   -l <logfile>  redirect output to logfile" << std::endl;
    std::cout << "   -h            this help" << std::endl;
    exit(0);
}

bool parseargs(int argc, char **argv, std::string &config)
{
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (argv[i][1] == 'd')
                isdaemon = true;
            if (argv[i][1] == 'h')
                usage(argv[0]);
            if (argv[i][1] == 'l')
                if (i < argc - 1)
                    logfile(argv[++i]);
        }
        else
        {
            config = argv[i];
            break;
        }
    }

    return true;
}

std::string find_configuration(std::string filename)
{
    for (std::string prefix: config_paths) {
        std::string tmp = prefix + filename;
        if (tmp[0] == '~')
            tmp.replace(0, 1, getenv("HOME"));
        if (std::ifstream(tmp))
            return tmp;
    }

    return "";
}

int main(int argc, char **argv)
{
    std::string filename = find_configuration("tinysoul.conf");
    parseargs(argc, argv, filename);

    log() << "Welcome in Tinysoul" << std::endl;
    log() << "Loading configuration..." << std::endl;

    Config cfg(filename);
    if(!cfg.check())
        die("Informations missing");
    while(true)
    {
        try
        {
            link(cfg);
        }
        catch (const char *msg)
        {
            log() << msg << std::endl;
        }

        log() << "Next retry in 5 secs" << std::endl;
        sleep(RECONNECTION_TIMEOUT);
    }
}
