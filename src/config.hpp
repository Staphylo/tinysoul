#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <cstdint>

struct Config
{
    Config(std::string filename);
    bool check() const;

    uint16_t port;
    std::string server;
    std::string login;
    std::string password;
    std::string location;
    std::string data;
    std::string logfile;
};

#endif /* !CONFIG_H */

